# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Classes and functions for compression.

Currently implements:
Compressor      --      an abstract class to implement common interface
Packer          --      a class for simple word packing
Huffman         --      a class for huffman compression
SymbolError     --      raised if a symbol could not be found
CodeError       --      raised if a code could not be found

A compression class should implement the common interface:
encode([plain]) -> [crypt]       -- encode a sequence of symbols and return a list of codes
decode([crypt]) -> [plain]       -- decode a sequence of codes and return a list of symbols

All compressors should raise SymbolError and CodeError exceptions
if a symbol / code could not be found during encoding / decoding
"""

import operator as op
from math import *
from abstract import *

__all__ = ["compressor","packer","huffman","SymbolError","CodeError"]


##########################################
# exceptions
##########################################

class SymbolError(Exception): pass
class CodeError(Exception): pass

##########################################
# compressor class
# parent of all compression classes
##########################################

class compressor (abstract):
    """Abstract compressor class

    Provides a common interface for compressor classes
    No member fields.
    """
    def encode (self,plain):
        """encode(plain) -> plain"""
        return plain

    def decode (self,crypt):
        """decode(crypt) -> crypt"""
        return crypt

##########################################
# packer class
# word packing
##########################################

class packer (compressor):
    """A class for simple word packing

    Takes a range and word size and tries to pack as many values of the
    predefined range into one word.
    
    Memebers
    _wsize  --  the word size in bits
    _range  --  the range of acceptable values, range() or xrange() object
    _len    --  the number of distinc values in range
    _rate   --  the compression rate, how many values per word
    """

    def __init__ (self,wsize,rng):
        """Instantiate a word package.

        @wsize  --  the word size in number of bits
        @rng    --  a range of values. use range(x) or xrange(x)
        """
        if type(rng) != type(xrange(0)) and type(rng) != type(range(0)):
            raise TypeError, 'range must be of type [] or xrange'
        self._wsize = wsize
        self._range = rng
        self._len = len(self._range)
        self._rate = self._calcrate()

    def __repr__ (self):
        return 'packer(%s,%s)' % (self._wsize,self._range)
        
    def _calcrate (self):
        """Calculate the number of values that can be packed into one word"""
        return int(floor(log(2**self._wsize,2)/log(self._len,2)))

    def _encode (self,values):
        """_encode([values]) -> word

        Encode a list of values into one word
        The list must contain a number of values less or equal to the pack rate
        @values  --  a sequence of values
        @return  ->  a compressed word
        """
        if len(values) > self._rate:
            raise self._error, 'Too many values to encode.'
        exp = 0
        word = 0
        for x in values:
            if x not in self._range:
                raise SymbolError, 'value %s not in range' % x
            word += int((x-min(self._range)) * self._len**exp)
            exp += 1
        return word

    def _decode (self,word):
        """_decode(word) -> [values]

        Decode a word
        Takes a word of type int or long and decodes it into corresponding values
        @word   --  the word to decode
        @return ->  a list of values
        """
        if type(word) != type(0) and type(word) != type(0L):
            raise TypeError, 'word is not of type int or long'
        x = []
        for i in xrange(self._rate):
            x.append((word % self._len) + min(self._range))
            word /= self._len
        return x

    def _codebook (self):
        """_codebook() -> [()]

        Return the codebook
        Calculates the codebook by encoding all possible words.
        Can be potentially very large.
        @return -> the codebook
        """
        x = []
        for i in xrange(self._len**self._rate):
            x.append((i,self._decode(i),self._encode(self._decode(i))))
        return x

    def decode (self,codes):
        """decode([codes]) -> [values]

        Decode a sequence of codes
        Every code in the list is treated as a word and unpacked
        @codes  --  the codes to unpack
        @return ->  a list of values
        """
        x = []
        for word in codes:
            x.append(self._decode(word))
        return reduce(op.add,x)

    def encode (self,values):
        """encode([values]) -> [codes]

        Encode a sequence of values
        The values are packed into as many words as needed.
        The words are concatenated to a list and returned.
        @values  --   a list of values to pack
        @return  ->   a list of encoded word(s)
        """
        codes = []
        x = []
        for i in values:
            x.append(i)
            if len(x) == self._rate:
                codes.append(self._encode(x))
                x = []
        codes.append(self._encode(x))
        return codes

    def size (self,num):
        """size(num) -> size of num encoded values in bits

        Calculate the size in bits of a number of compressed values
        @num     --     the number of values
        @return  ->     the size of the compressed values in bits
        """
        return (num/float(self._rate)) * self._wsize


##########################################
# huffman class
# huffman compression
##########################################

class huffman (compressor):
    """A class for Huffman compression

    Takes a distribution which is a dictionary of the form
    { symbol : frequency } and calculates a Huffman tree.
    """
    
    class node:
        """A binary huffman tree node

        Members
        _symbol  --    the symbol represented by the node
        _freq    --    frequency of the symbol
        _left    --    left child
        _right   --    right child
        """
        def __init__ (self,s,f,l = None, r = None):
            """Instantiate a huffman tree node

            @s -- symbol
            @f -- frequency
            @l -- left child
            @r -- right child
            """
            self._symbol = s
            self._freq = f
            self._left = l
            self._right = r
            
        def __cmp__ (self, other):
            """__cmp__(self,other) -> cmp(self._freq, other._freq)"""
            return cmp(self._freq, other._freq)

        def __repr__ (self):
            return self._tostring(self,0,"")

        def __str__ (self):
            return self._tostring(self,0,"")

        def _tostring(self,leaf,depth,b):
            if not leaf: return ""
            s = ""
            for i in xrange(depth):
                if b=="0": s += "|<<"
                elif b=="1": s += "|>>"
            s += "(%s,%s):%s\n" % (leaf._symbol, leaf._freq,b)
            s += self._tostring(leaf._left, depth+1, "0")
            s += self._tostring(leaf._right, depth+1, "1")
            return s

        def __getitem__(self,index):
            node,index = self._getitem(index)
            if not node: raise IndexError
            return node

        def _getitem(self,index):
            if index == 0: return self,index
            node = None
            if self._left:
                node,index = self.left._getitem(index-1)
            if not node:
                if self._right:
                    node,index = self.right._getitem(index-1)
            return node,index

        class iterator(object):
            """Huffman tree node iterator."""
            def __init__(self, root):
                """Instantiate the iterator.

                The passed node @root gets index 0.
                Iterates top-down, left-right
                """
                self._root = root
                self._index = 0

            def __iter__(self):
                return self

            def next(self):
                try: item = self._root[self._index] 
                except IndexError: raise StopIteration 
                self.index += 1
                return item

        def __iter__(self): 
            return self.iterator(self)
        
    def __init__ (self,dist):
        """Instantiate a huffman compressor object

        Uses a { symbol : frequency } dictionary to calculate the
        Huffman tree and codebooks.
        @dist -- dictionary representing the frequency of symbols
        """
        if not isinstance(dist,dict): raise TypeError("Argument dist must be a dict")
        # build the tree
        self._root = self._buildtree(dist)
        # initialize code and symbol table
        self._codes = {}            # _codes:   { symbol : code }
        self._symbols = {}          # _symbols: { code : symbol }
        # build the codebooks
        self._codebook(self._root,"")

    def __repr__ (self):
        return "huffman: %s" % (repr(self._codes))

    def _buildtree(self,dist):
        """_buildtree(dist) -> huffman tree

        Based on the frequency of symbols calculate a huffman tree
        """
        # populate the forest, one tree for every symbol
        forest = sorted([self.node(s,dist[s]) for s in dist],reverse=True)
        # while there are more than one trees, aggregate the highest nodes
        while len(forest)>1:
            rtree = forest.pop()
            ltree = forest.pop()
            forest.append(self.node(None,ltree._freq+rtree._freq,ltree,rtree))
        return forest[0]

    def _codebook (self,node,code):
        """_codebook(node,code) -- calculates symbol and code table

        Start with @node and @code = '' and iteratively calculates the code
        for any symbol. The results are directly put into _symbols, _codes.
        """
        if node._symbol != None:
            if code == "": code += "0"
            else:
                self._symbols[code] = node._symbol
                self._codes[node._symbol] = code
        else:
            if node._left: self._codebook(node._left,code+"0")
            if node._right: self._codebook(node._right,code+"1")

    def encode (self,symbols):
        """encode([symbols]) -> [codes]

        Encode a list of symbols.
        @return  ->  a list of codes.
        """
        codes = []
        for s in symbols:
            try: codes.append(self._codes[s])
            except (KeyError): raise SymbolError, "symbol %s not found" % (s)
        return codes

    def decode (self,codes):
        """decode([codes]) -> [symbols]

        Decode a list of codes.
        @return  ->  a list of symbols
        """
        symbols = []
        for c in codes:
            try: symbols.append(self._symbols[c])
            except (KeyError): raise CodeError, "code %s not found" % (c)
        return symbols

def hist (items):
	"""hist([items]) -> { item : frequency }

	Calculates a histogram of the frequency of items
	in the sequence [items].
	"""
	h = {}
	for i in items:
		try: h[i] += 1
		except (KeyError): h[i] = 1
	return h

if __name__ == '__main__':
    plain = [1,3,4,2,3,4,5,4,3,2,3,4,3,2,2,1,1,2,3,4,5,4,3,2,3,2,2,3,4,4,4,3,2,5]
    print plain
    c = packer(8,range(max(plain)+1))
    crypt = c.encode(plain)
    print crypt
    print c.decode(crypt)

    c = huffman(hist(plain))
    crypt = c.encode(plain)
    print crypt
    print c.decode(crypt)
    
