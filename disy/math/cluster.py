# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""cluster.py

Implements clusters and clustering algorithms.
Clusters are basically lists of vectors, similar to matrices, but providing
a different API. A clustering is a list of clusters.

The following clustering algorithms are implemented:
k-means [3]
GenIc incremental leader [4]
agglomorative clustering [5]

In addition a cluster aggregation algorithm [1] is implemented. 

All clustering algorithms take the following parameters
dist -- distance or similarity function
comp -- a comparator, used to indicate whether lower values or higher values
				produced by the distance/similarity function mean higher similarity.
				use -1 if lower is better, 1 if higher is better. In case of similarity
				higher usually means more similar, however, in case of distances, the
				higher the distance, the less the similarity.
				use -1 for distance functions, +1 for similarity functions

References
[1] Hanan Ayad and Mohamed S. Kamel.
		"Topic discovery from text using aggregation of different clustering
		methods". In AI ’02: Proceedings of the 15th Conference of the Canadian
		Society for Computational Studies of Intelligence on Advances in
		Artificial Intelligence, pages 161–175, London, UK, 2002. Springer-Verlag.
		
[2] M. F. Porter.
		"An algorithm for suffix stripping". Readings in information retrieval,
		pages 313–316, 1997. http://tartarus.org/martin/PorterStemmer/.

[3] Lloyd, S. P.
		"Least square quantization in PCM". Bell Telephone Laboratories, 1957. 
		IEEE Transactions on Information Theory 28 (2): 129–137.
		doi:10.1109/TIT.1982.1056489

[4] Chetan Gupta and Robert Grossman.
		"GenIc: A Single Pass Generalized Incremental Algorithm for Clustering",
		In SIAM Int. Conf. on Data Mining, 2004

[5] http://en.wikipedia.org/wiki/Cluster_analysis, retrieved June '11
"""
__all__ = ["cluster",'leader', 'kmeans', 'agglomorate', 'average_linkage', 'single_linkage', 'complete_linkage', 'threshold', 'distance_matrix', 'aggregate']

from vector import *
import random, sys

class cluster(object):
	'''class for cluster representation
	
	Members:
	points: a list of vectors
	n: the number of dimensions
	centroid: the centroid
	topic: a topic os description of the cluster
  '''
	def __init__(self, vectors):
		self.vectors = []
		self.n = len(vectors[0])
		for v in vectors:
			if len(v) != self.n: raise Exception("Multiple Dimensions Exception")
			self.vectors.append(vector(v))
		self.centroid = mean(self.vectors)

	def __repr__(self):
		return str(self.vectors)

	def __str__(self):
		return str(self.vectors)

	def __len__(self):
		return len(self.vectors)

	def __getitem__(self, i):
		return self.vectors[i]
	
	#def __setitem__(self,i,v):
	#	if len(v) != self.n: raise Exception("Multiple Dimensions Exception")
	#	self.vectors[i] = v
	#	self.centroid = mean(self.vectors)

	def put(self, v):
		if len(v) != self.n: raise Exception("Multiple Dimensions Exception")
		self.vectors.append(vector(v))
		self.centroid = mean(self.vectors)

	def pop(self, index):
		r = self.vectors.pop(index)
		self.centroid = mean(self.vectors)
		return r

	def remove(self, v):
		r = self.vectors.remove(v)
		self.centroid = mean(self.vectors)
		return r

	def update(self, vectors):
		self.vectors = vectors
		self.centroid = mean(self.vectors)

	def settopic(self, topic):
		self.topic = topic
# cluster()
 
# clustering algorithms

def leader(vectors, t, dist, comp, progress=False):
	'''leader
	implements the incremental leader algorithm. It works as follows:
	1. Select first vector as cluster centroid
	2. Select next vector and compare to centroids.
		If similarity is below threshold t, form a new cluster.
		Else assign vector to the nearest centroid and recompute centroid.
	3. While still vectors goto 1.
	
	Parameters:
	points -- the list of vectors to cluster
	t -- similarity/distance threshold for forming a new cluster
	dist -- the distance function to use
	comp -- the comparator
	
	Return:
	a list of clusters
	'''
	f = max if comp==1 else min 
	clusters = [cluster([vectors[0]])]
	for v in vectors[1:]:
		distances = []
		for c in clusters:
			distances.append(dist(v, c.centroid))
		if cmp(t,f(distances)) == comp:
			clust = cluster([v])
			clusters.append(clust)
			if progress: print '*',; sys.stdout.softspace = 0
		else:
			clust = clusters[distances.index(f(distances))]
			clust.put(v)
			if progress: print '.',; sys.stdout.softspace = 0
	return clusters
# leader

def kmeans(vectors, k, dist, comp, progress=False):
	'''kmeans
	Implements k-means cluster algorithm. It 	tries to minimize the sum of
	square errors.
	1. Select k vectors as initial centroids
	2. Assign all vectors to nearest centroid and recompute centroid
	3. Until no further change goto 1.
	
	Parameters:
	vectors -- list of vectors to cluster
	k -- number of clusters
	dist -- similarity/distance function to use
	comp -- the comparator
	
	Return:
	a list of clusters
	'''
	# randomly assign k points as initial clusters
	initial = random.sample(vectors, k)
	clusters = []
	for p in initial: clusters.append(cluster([p]))
	changed = True
	while changed:
		if progress: print '|',; sys.stdout.softspace = 0
		# a list of points for each cluster
		lists = []
		for i in range(k): lists.append([])
		for p in vectors:
			# find closest centroid
			cnum = 0
			closest = dist(p, clusters[cnum].centroid)
			for i in range(1,k):
				distance = dist(p, clusters[i].centroid)
				if cmp(distance,closest) == comp:
					closest = distance
					cnum = i
			# add point to cluster's list
			lists[cnum].append(p)
		# update each Cluster with corresponding list
		notchanged = 0
		for i in range(k):
			if clusters[i].vectors == lists[i]:
				notchanged +=1
				if progress: print '.',; sys.stdout.softspace = 0
			else:
				clusters[i].update(lists[i])
				if progress: print '*',; sys.stdout.softspace = 0
		# check break conditions
		if notchanged == k: changed = False
	return clusters
# kmeans


def agglomorate(vectors, linkage, t, dist, comp, progress=False):
	'''agglomorative link
	
	Implements the hierarchical agglomorative complete-link algorithm.
	1. Treat every vector as cluster centroid
	2. Merge similar cluster
	3. Goto 2 until only one cluster left, or similarity threshold reached
	
	Parameters:
	vectors -- list of vectors to cluster
	link -- the linkage function to use
	t -- similarity threshold
	dist -- similarity / distance function to use
	comp -- comparator
	'''
	clusters = []
	for v in vectors: clusters.append(cluster([v]))
	while True:
		if progress: print '.',; sys.stdout.softspace = 0
		dm = distance_matrix(clusters, linkage, dist, comp)
		# find the similar clusters
		best_key = dm.keys()[0]
		best_dist = dm[best_key]
		for key in dm.keys():
			if cmp(dm[key],best_dist) == comp:
				best_key = key
				best_dist = dm[key]
		# compare to threshold and merge or break
		if cmp(t,best_dist) == comp: break
		else:
			c0, c1 = clusters[best_key[0]], clusters[best_key[1]]
			c = cluster(c0.vectors + c1.vectors)
			clusters.remove(c0)
			clusters.remove(c1)
			clusters.append(c)
			if progress: print '*',; sys.stdout.softspace = 0
		if len(clusters) == 1: break
	return clusters
# agglomorate()

def average_linkage(c0, c1, dist, comp):
	'''average linkage
	The mean distance between elements of each cluster.
	'''
	distance = 0
	for i in c0.vectors:
		for j in c1.vectors:
			distance = distance + dist(i,j)
	return (1. / (len(c0) * len(c1)) * distance)

def single_linkage(c0, c1, dist, comp):
	'''single linkage
	The minimum distance between elements of each cluster.
	'''
	d = dist(c0.vectors[0], c1.vectors[0])
	for i in c0.vectors:
		for j in c1.vectors:
			distance = dist(i,j)
			if cmp(distance, d) == comp: d = distance
	return d

def complete_linkage(c0, c1, dist, comp):
	'''complete linkage
	The maximum distance between elements of each cluster.
	'''
	d = dist(c0.vectors[0], c1.vectors[0])
	for i in c0.vectors:
		for j in c1.vectors:
			distance = dist(i,j)
			if cmp(distance, d) == -comp: d = distance
	return d
	
def distance_matrix(clusters, linkage, dist, comp):
	'''Calculates a distance matrix of a list of clusters.'''
	dm = dict()
	l = len(clusters)
	for i in range(l):
		for j in range(l):
			if j == i: break
			dm[(i,j)] = linkage(clusters[i],clusters[j],dist,comp)
	return dm

def threshold(vectors, a, dist):
	"""
	Calculates a stopping threshold based on harmonised average distances.
	Intended for agglomorative clustering and cluster aggregation, but 
	suitable for a number of clustering algorithms.
	
	Parameters:
	vectors -- vectors in initial vector space
	a -- arbitrary coefficient, 0 < a <= 1
	dist -- distance / similarity function
	
	Return:
	the calculated distance / similarity threshold
	"""
	t, N = 0, len(vectors) 
	den = float((N**2-N)/2)
	for i in vectors:
		for j in vectors:
			if i==j: break
			t += a*(dist(i,j)/den)
	return t


def aggregate(A, B, t, dist, comp, progress=False):
	'''Cluster Aggregation
	Implement the cluster aggregation algorithm proposed by [1].
	The algorithm takes two clusterings (list of clusters) and incrementally
	merges similar clusters until only one clustering remains.
	
	It works as follows
	1. initialize combined clustering G to empty set
	2. for each cluster A[i] and cluster B[j] compute intersection set I
	2.1 if both clusers are identical
		- G += I
	2.2 one cluster is the subset of the other
		- compute similarity s of each element in set difference to center of I
		- merge all elements where s >= t with I, add remaining to J
		- G += I, G += J
	2.3 both clusters intersect
		- compute centers of both set differences and similarity s
		- if s>=t merge A[i] and B[j] and add to G
		- else merge the elements in I with most similar set difference and
			add both sets to G
	
	Complexity is O(n^2) with high memory requirements. The algorithm is not
	invariant to cluster ordering, so changing cluster sequence will lead
	different results. Also, there is no handling of clusters that have an empty
	intersection set.
	
	Parameters:
	A -- first list of clusters
	B -- second list of clusters
	t -- aggregation threshold
	dist -- distance / similarity function
	comp -- comparator
	
	Return:
	an aggregated list of clusters
	'''
	G = []
	processed = 0
	for i in range(len(A)):
		if progress: print 'a',; sys.stdout.softspace = 0
		a = frozenset(A[i])
		for j in range(len(B)):
			if progress: print 'b',; sys.stdout.softspace = 0
			if (processed >> j) & 1: continue
			b = frozenset(B[j])
			I = a & b
			print "\na:",a, "\nb:",b, "\nI:", I
			if I != set():
				dAB = a-b
				dBA = b-a
				# case 0 a == b
				if a == b:
					if progress: print '=',; sys.stdout.softspace = 0
					if I not in G: G.append(I)
				# case 1 a <= b xor b <= a
				if (a<b) ^ (b<a):
					if progress: print '^',; sys.stdout.softspace = 0
					J = frozenset([]) 
					c0 = cluster(list(I)).centroid
					D = dAB if dAB != set() else dBA
					for p in D:
						if cmp(dist(p, c0), t) in [0, comp]: I ^= frozenset([p])
						else: J ^= frozenset([p])
					if I not in G: G.append(I)
					if J != set() and J not in G: G.append(J)
				# case 2 a-b
				if not a<=b and not b<=a:
					if progress: print '!',; sys.stdout.softspace = 0
					c1 = cluster(list(dAB)).centroid
					c2 = cluster(list(dBA)).centroid
					if cmp(dist(c1,c2), t) in [0, comp]:
						uni = a | b
						if uni not in G: G.append(uni)
					else:
						for p in I:
							s1 = dist(p,c1)
							s2 = dist(p,c2)
							if cmp(s1,s2)==comp: dAB |= frozenset([p])
							elif cmp(s2,s1)==comp: dBA |= frozenset([p])
							elif s1==s2:
								dAB |= frozenset([p])
								dBA |= frozenset([p])
							if dAB not in G: G.append(dAB)
							if dBA not in G: G.append(dBA)
				processed |= 1 << j
	G = [cluster(list(i)) for i in G]
	return G
  

if __name__ == "__main__":
	nump,n,lower,upper = 10,2,-10,10
	vectors = []
	print "VECTORS\n"
	for i in range(nump):
		invalid = True
		while invalid:
			v = random_vector(n,lower,upper)
			if norm(v) != 0 and v not in vectors:
				vectors.append(v)
				print str(vectors[i]) + ",",
				invalid = False
	print vectors
	
	print
	print "\nLEADER ALGORITHM\n"
	t = 0.65
	print "using cosine Similarity, t = " + str(t)
	cleader0 = leader(vectors, t, cosine, 1, True)
	print
	for c in cleader0: print str(c)
	print
	t = 7
	print "using euclidean distance, t = " + str(t)
	cleader1 = leader(vectors, t, euclid, -1, True)
	print
	for c in cleader1: print str(c)
	
	print
	print "\nKMEANS ALGORITHM\n"
	k = 3
	print "using cosine similarity, k = " + str(k)
	ckmeans0 = kmeans(vectors,k,cosine,1,True)
	print
	for c in ckmeans0: print str(c)
	print
	print "using euclidean distance, k = " + str(k)
	ckmeans1 = kmeans(vectors,k,euclid,-1,True)
	print
	for c in ckmeans1: print str(c)
	
	print
	print "\nAVERAGE LINKAGE ALGORITHM\n"
	t = threshold(vectors,.4,cosine)
	print "using cosine similarity, t = " + str(t)
	cagglo = agglomorate(vectors, average_linkage, t, cosine, 1, True)
	print
	for c in cagglo: print str(c)
	print
	t = threshold(vectors,.8,euclid)
	print "using euclidean distance, t = " + str(t)
	cagglo = agglomorate(vectors, average_linkage, t, euclid, 1, True)
	print
	for c in cagglo: print str(c)
	
	print
	print "\nSINGLE LINKAGE ALGORITHM\n"
	t = threshold(vectors,.4,cosine)
	print "using cosine similarity, t = " + str(t)
	cagglo = agglomorate(vectors, single_linkage, t, cosine, 1, True)
	print
	for c in cagglo: print str(c)
	print
	t = threshold(vectors,.8,euclid)
	print "using euclidean distance, t = " + str(t)
	cagglo = agglomorate(vectors, single_linkage, t, euclid, 1, True)
	print
	for c in cagglo: print str(c)
	
	print
	print "\nCOMPLETE LINKAGE ALGORITHM\n"
	t = threshold(vectors,.4,cosine)
	print "using cosine similarity, t = " + str(t)
	cagglo = agglomorate(vectors, complete_linkage, t, cosine, 1, True)
	print
	for c in cagglo: print str(c)
	print
	t = threshold(vectors,.8,euclid)
	print "using euclidean distance, t = " + str(t)
	cagglo = agglomorate(vectors, complete_linkage, t, euclid, 1, True)
	print
	for c in cagglo: print str(c)
	
	print
	print "\nCLUSTER AGGREGATION ALGORITHM\n"
	t = threshold(vectors, .5, cosine)
	print "using cosine similarity, t = " + str(t)
	combined = aggregate(cleader0, ckmeans0, t, cosine, 1, True)
	for c in combined: print str(c)
	print
