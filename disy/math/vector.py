# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Vector Space

Implements n-dimensional vectors, provides additional math functions for 
sequences and distance functions for vectors. The implementation is influenced
by the vec.py example of Python 2.5.
"""
__all__ = ["vector",'avg','norm','unit','dot','mean','cosine','euclid','manhattan','maximum','lmetric','delta','random_vector']

import random

class vector (object):
	"""Vector class
	
	Implements a n-dimensional vector in a vector space.
	"""
	def __init__(self, data):
		"""__init__(iterable) -- initialize the vector instance
		
		data 	-- 	an iterable sequence used to initialize the vector
		"""
		self._data = list(data)

	def __repr__(self):
		#return '<' + repr(self._data)[1:-1] + '>'
		return 'vector(' + repr(self._data) + ')'

	def __str__(self):
		#return '<' + repr(self._data)[1:-1] + '>'
		return 'vector(' + repr(self._data) + ')'

	def __len__(self):
		return len(self._data)

	def __getitem__(self, i):
		return self._data[i]

	def __setitem__(self,i,v):
		self._data[i] = v

	def __add__(self, other):
		"""__add__(...) -- element-wise addition"""
		return vector(map(lambda x, y: x+y, self, other))

	def __sub__(self, other):
		"""__sub__(...) -- element-wise subtraction"""
		return vector(map(lambda x, y: x-y, self, other))

	def __mul__(self, scalar):
		"""___mul__(...) -- multiplication with scala
		to multiply vectors use the functions dot or cross"""
		return vector(map(lambda x: x*scalar, self))

	def __div__(self, scalar):
		"""___div___(...) -- divide by a scala"""
		return vector(map(lambda x: x/float(scalar), self))

	def __pow__(self, scalar):
		"""___pow___(...) -- calc the power using a scalar"""
		return vector(map(lambda x: x**scalar, self))

	def __abs__(self):
		# element-wise absolute value
		return vector(map(lambda x: abs(x), self))

	def __cmp__(self,other):
		return cmp(self._data, other._data)

	def __hash__(self):
		#return id(self)
		return hash(self.__repr__())

	def append(self, v):
		"""append(v) -- append new dimension with weight 1"""
		if (len(self) > v): self[v] = self[v] + 1
		else: self.append(1)

	def fill(self,v):
		while (len(self) < v):
			self.append(0)

	# set a reference for the vector
	def reference(self,ref):
		"""reference(ref) -- sets a reference for the vector"""
		self.ref = ref
# vector()

# calculation functions for lists and vectors

def avg(sequence):
	"""avg(sequence) -- calculates the average of the sequence"""
	return sum(sequence)/float(len(sequence))

def norm(sequence):
	"""norm(sequence) -- calculates the norm of the sequence"""
	#return sum(map(lambda x: x**2, sequence))**.5
	return sum([x**2 for x in sequence])**.5

def unit(sequence):
	"""unit(sequence) -- calculates the unit vector with norm 1"""
	#return vector(map(lambda x: x/norm(sequence), sequence))
	return vector([x/norm(sequence) for x in sequence])
	
def dot(seq0, seq1):
	"""dot(sequence0, sequence1) -- calculates the dot product of two sequences"""
	return sum(map(lambda x,y: x*y, seq0, seq1))

def mean(vectors):
	"""mean(vectors) -- calculates the mean of multiple vectors"""
	s = vector([0]*len(vectors[0]))
	for v in vectors: s += v
	return s/float(len(vectors))

# distance function

def cosine(v0,v1):
	"""cosine(x,y) -- cosine similarity"""
	return dot(v0,v1)/(norm(v0)*norm(v1))

def euclid(v0,v1):
	"""euclid(x,y) -- euclidean distance"""
	return sum((v0-v1)**2)**.5

def manhattan(v0,v1):
	"""manhatten(x,y) -- manhatten distance"""
	return sum(abs(v0-v1))

def maximum(v0,v1):
	"""maximum(x,y) -- maximum distance"""
	return max(abs(v0-v1))

def lmetric(v0,v1,p):
	"""lmetric(x,y,p) -- lmetric distance"""
	return sum(abs(v0-v1)**p)**(1/float(p))

def delta(v0,v1):
	"""delta(x,y) -- number of different elements"""
	return sum(map(lambda x,y: 0 if x==y else 1, v0, v1))

# unsorted functions

def random_vector(n, lower, upper):
	"""random_vector(n,lower,upper) -- creates a random vector with n dimensions and lower / upper bounds"""
	v = []
	for i in range(n): v.append(int(round(random.uniform(lower,upper),0)))
	return vector(v)

# main

if __name__ == "__main__":
	n,lower,upper = 5,-10,10
	a = vector([5,3,8,7,5,3])
	b = vector([9,4,6,3,1,8])
	print "a:", a
	print "b:", b
	print "a==vector([5,3,8,7,5,3]):", a == vector([5,3,8,7,5,3])
	print "a+b =", a+b
	print "a-b =", a-b
	print "a dot b =", dot(a,b)
	print "a*3 =", a*3.0
	print "b/2 =", b/2.0
	print "sum(a) =", sum(a)
	print "avg(b) =", avg(b)
	print "norm(a) =", norm(a)
	print "unit() =", unit(b)
	print "mean([a,b]) =", mean([a,b])
	print "cosine(a,b) =", cosine(a,b)
	print "euclid(a,b) =", euclid(a,b)
	print "manhattan(a,b) =", manhattan(a,b)
	print "maximum(a,b) =", maximum(a,b)
	print "delta(a,b) =", delta(a,b)
