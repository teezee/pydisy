# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Classes and Functions that provide syllabification

Implements an abstract syllabifier class and a simple CV syllabifier. A 
syllabifier takes a sequence of words and exctracts the syllables for each
of the words. It returns a dictionary mapping strings to their syllables. This
action is known as syllabification. There are a few syllabification algorithms
one of which is implemented here. 

All syllabifiers should be derived from the abstract syllabifier base class
which defines a common interface and wrapper methods. Syllabifiers must
implement the method _syllabify, which takes a string and performs the
syllabification.

The syllabification is performed on a sequence of words by calling the
syllabifier directly. Assumes normalized input and does not ignore case.

Implements the following classes:
syllabifier          --          abstract base class for all syllabifiers
SimpleSyllabifier    --          only works on CV sequences

Example
>>> from disy.text import *
>>> s = SimpleSyllabifier('aeiouy')
>>> s("Hello world of syllabification".split(" "))
{'syllabification': ['syl', 'la', 'bi', 'fi', 'ca', 'tion'],
 'world': ['world'],
 'Hello': ['Hel', 'lo'],
 'of': ['of']}
"""

from disy.util.abstract import *

__all__ = ["syllabifier","SimpleSyllabifier"]

class syllabifier (abstract):
    """Abstract Base Class for Syllabifiers

    Defines a common interface. A syllabifier takes a list
    of vowels as arguments for instantiation. If additional
    arguments must be set, reasonable defaults should be provided.
    The syllabifier can be called directly via the __call__ funtion.
    It takes a sequence of words as argument. Each word is then syllabified
    and a dictionary of words and syllables returned.
    """
    _name_ = "ABCSyllabifier"
    
    def __init__ (self,vowels):
        """Instantiate a Syllabifier

        Parameters:
        vowels         --      a sequence of vowels
        """
        self._vowels = vowels

    def __repr__ (self):
         return "%s(%s)" % (self._name_,id(self))
        
    def __call__ (self,words):
        """__call__ (words) -> { word : [syllables] }

        Does syllabification on each word in the sequence of words.
        """
        syllables = {}
        for word in words:
            try: syllables[word]
            except (KeyError): syllables[word] = self.syllabify(word,0,len(word))
        return syllables

    def syllabify (self,p,i=0,j=0):
        """syllabify(p,i,j) -> string, syllabify the input string p from position i to j"""
        if j <= i+1: return p
        return self._syllabify(p[i:j])

    def _syllabify (self,p):
        abstract()


class SimpleSyllabifier (syllabifier):
    """Identify syllables by vowels.

    This syllabifier just searches for vowels with preceding
    consonants. The word is split into syllables right before CV.
    """
    _name_ = "SimpleSyllabifier"
    
    def _syllabify (self,p):
        cur = p[0]
        iscluster = False if cur not in self._vowels else True
        for c in p[1:]:
            if c in self._vowels:
                if iscluster:
                    if cur[-1] not in self._vowels:
                        cur = cur[:-1]+ '.'+cur[-1]
                iscluster = True
            cur += c
        return cur.split('.')


#class AutoSyllabifier (syllabifier):
#    """Identify syllables by consonant clusters.
#
#    Searches for initial (onset) and final (coda) consonant clusters.
#    Words are split on the longest possible onsets.
#    """
#    _name_ = "AutoSyllabifier"
