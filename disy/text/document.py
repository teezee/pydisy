# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Document Analysis

Implements classes and functions used for document analysis.
"""

__all__ = ['tfidf','tficf']

def tfidf_coef(tf):
    '''tfidf_coef(int) -> float
    depending on the term frequency, calculates the coefficient
    for tfidf.
    
    tf -- term frequency
    '''
    if (tf==0):
        return 0
    if (tf==1):
        return 1
    if (tf>1 and tf <=5):
        return 1.5
    if (tf>5 and tf <=10):
        return 2
    else:
        return 2.5

def tfidf(tf,n,df,f=tfidf_coef):
    '''calculates the term frequency - inverse document frequency

    tf  -- term frequency
    n   -- number of documents
    df  -- document frequency, the number of documents that have the term
    f   -- function to calculate the term frequency weight / coefficient
    '''
    return f(tf)*(math.log10(n)-math.log10(df)+1)

def tficf(tf,n,cf):
    '''calculates the term frequency - inverse cluster frequency

    tf  -- term frequency
    n   -- number of clusters
    cf  -- cluster frequency, the number of clusters that have the term
    '''
    return tf*(math.log10(n)-math.log10(cf))
