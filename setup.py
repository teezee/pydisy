#!/usr/bin/env python

from distutils.core import setup
import disy

setup(name='disy',
      version=disy.__version__,
      author=disy.__author__,
      url=disy.__url__,
      description='simple python add on library',
      packages=['disy','disy.math','disy.net','disy.text','disy.util'],
)
