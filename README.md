FAIR LICENSE

Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.


# PYDISY - simple python add on library

## I. Introduction

This is a simple collection of some useful add-ons for python. It includes
classes related to cluster and text analysis, network packet decoding, as
well as certain object oriented patterns that are not standard in python.

Note that the implementations are pretty basic and far from feature complete.
For most of the stuff covered here there exist sophisticated libraries
like SciPy which covers vectors or NLTK which covers everything related
to language processing. The code presented was mostly generated for
seminars or other research projects which required fast and easy
implementations. And that's what it is targeted at. If you do not need
or want full fledged libraries that present a lot of overhead and a steep
learning curve but just want to do some basic vector arithmetic, compute
some distances, do basic text analysis with stemming and syllabification,
this frameworks gives you the tools you need. Also,the abtract and single-
ton implementations have been proven quite useful and I personally use
them ever since for basically all my python projects.

Some of the code is inspired by other sources and authors which are credited
where appropriate and the original author can be tracked. Sometimes, however,
it isn't possible to find the original author without much effort. If you
think, that you are the original author of code presented herein, feel free
to contact me.


## II. Modules

The following modules are included in the disy package.

- util
	Implements abstract classes and functions, a singleton,
	as well as typed classes

- math
	lightweight vector class and functions, including distance / similarity
	functions. Also cluster analysis related classes and functions.
	The vector implementation is pretty basic and can be done much better
	(as in fact is done in scipy). It's maily used for convenience in
	cluster analysis.

- net
	an implementation of a decoder for cisco's CHDLC protocol on top ofDug 
	Song's dpkt library. Note that dpkt is required.

- text
	text analysis, currently abstract classes and implementations of stemmers
	and syllabifiers.


## III. Dependencies

chdlc.py in the 'net' module depends on Dug Song's dpkt library. If the
library is not installed chdlc.py will not be imported and a message like
the following will appear:

	"Couldn't import chdlc: No module named dpkt"


## IV. Compile and use

To install the library run

	$ python setup.py install

To include it in your scripts/ programs just type

	>>> import disy

For help you can call pythons help() function on the package and modules. Or use docpython to generate python documentation files.
